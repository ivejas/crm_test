import { Session } from 'meteor/session'

Template.mainLayout.helpers({
    loggedIn: function() {
        if(Meteor.userId() != null) {
            return true;
        }
        return false;
    },
    isActive: (page) => {
        if(page == Session.get('currentPage')) {
            return true;
        }
        return false;
    },
    getUserName: () => {
        if(Meteor.user() != null) {
            return Meteor.user().profile.first_name + ' ' + Meteor.user().profile.last_name;
        }
        return false;
    }
});

Template.mainLayout.events({
    'click .logout': (event) => {
        Meteor.logout(() => {
            FlowRouter.go('/');
        });
    }
});
