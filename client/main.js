import '../imports/startup/routes.js';

import '../imports/ui/pages/login/login.js';
import '../imports/ui/pages/register/register.js';

import '../imports/ui/pages/products/index/productsIndex.js';
import '../imports/ui/pages/products/edit/productsEdit.js';
import '../imports/ui/pages/products/add/productsAdd.js';
import '../imports/ui/pages/products/view/productsView.js';

import '../imports/ui/pages/customers/index/customersIndex.js';
import '../imports/ui/pages/customers/add/customersAdd.js';
import '../imports/ui/pages/customers/edit/customersEdit.js';
import '../imports/ui/pages/customers/view/customersView.js';

import '../imports/ui/pages/orders/index/ordersIndex.js';
import '../imports/ui/pages/orders/add/ordersAdd.js';
import '../imports/ui/pages/orders/view/ordersView.js';
import '../imports/ui/pages/orders/edit/ordersEdit.js';

import '../imports/ui/pages/profile/profile.js';