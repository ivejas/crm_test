import { Template } from 'meteor/templating';

FlowRouter.route('/', {
    name: 'CRM.login',
    action(params, queryParams) {
        if(Meteor.userId() == null) {
            BlazeLayout.render('mainLayout', {main: 'login'});
        } else {
            BlazeLayout.render('mainLayout', {main: 'ordersIndex'});
        }
    }
});

FlowRouter.route('/register', {
    name: 'CRM.login',
    action(params, queryParams) {
       BlazeLayout.render('mainLayout', {main: 'register'});
    }
});

FlowRouter.route('/products', {
    name: 'CRM.login',
    action(params, queryParams) {
       BlazeLayout.render('mainLayout', {main: 'productsIndex'});
    }
});

FlowRouter.route('/products/add', {
    name: 'CRM.login',
    action(params, queryParams) {
       BlazeLayout.render('mainLayout', {main: 'productsAdd'});
    }
});

FlowRouter.route('/products/edit/:id', {
    name: 'CRM.login',
    action(params, queryParams) {
       BlazeLayout.render('mainLayout', {main: 'productsEdit'});
    }
});


FlowRouter.route('/products/view/:id', {
    name: 'CRM.login',
    action(params, queryParams) {
       BlazeLayout.render('mainLayout', {main: 'productsView'});
    }
});

FlowRouter.route('/orders', {
    name: 'CRM.login',
    action(params, queryParams) {
       BlazeLayout.render('mainLayout', {main: 'ordersIndex'});
    }
});

FlowRouter.route('/orders/add', {
    name: 'CRM.login',
    action(params, queryParams) {
       BlazeLayout.render('mainLayout', {main: 'ordersAdd'});
    }
});

FlowRouter.route('/orders/edit/:id', {
    name: 'CRM.login',
    action(params, queryParams) {
       BlazeLayout.render('mainLayout', {main: 'ordersEdit'});
    }
});

FlowRouter.route('/orders/view/:id', {
    name: 'CRM.login',
    action(params, queryParams) {
       BlazeLayout.render('mainLayout', {main: 'ordersView'});
    }
});

FlowRouter.route('/customers', {
    name: 'CRM.login',
    action(params, queryParams) {
       BlazeLayout.render('mainLayout', {main: 'customersIndex'});
    }
});

FlowRouter.route('/customers/edit/:id', {
    name: 'CRM.login',
    action(params, queryParams) {
       BlazeLayout.render('mainLayout', {main: 'customersEdit'});
    }
});

FlowRouter.route('/customers/view/:id', {
    name: 'CRM.login',
    action(params, queryParams) {
       BlazeLayout.render('mainLayout', {main: 'customersView'});
    }
});

FlowRouter.route('/customers/add', {
    name: 'CRM.login',
    action(params, queryParams) {
       BlazeLayout.render('mainLayout', {main: 'customersAdd'});
    }
});

FlowRouter.route('/profile', {
    name: 'CRM.login',
    action(params, queryParams) {
       BlazeLayout.render('mainLayout', {main: 'profile'});
    }
});