import { Mongo } from 'meteor/mongo';

export const Products = new Mongo.Collection('products');

Products.attachSchema({
    name: {
        type: String
    },
    width: {
        type: Number
    },
    height: {
        type: Number
    },
    weight: {
        type: Number
    },
    price: {
        type: Number
    },
    category: {
        type: String,
        allowedValues: ['chairs','tables','beds','sofas','benches']
    },
    added_by: {
        type: String
    },
    item_id: {
        type: Number
    }
});
