import { Mongo } from 'meteor/mongo';

export const Customers = new Mongo.Collection('customers');

Customers.attachSchema({
    _id: {
        type: String
    },
    first_name: {
        type: String
    },
    last_name: {
        type: String
    },
    phone: {
        type: String
    },
    email: {
        type: String
    },
    address: {
        type: String
    },
    city: {
        type: String
    },
    state: {
        type: String,
        optional: true
    },
    country: {
        type: String
    },
    added_by: {
        type: String
    },
    customer_id: {
        type: Number
    }
    
});
