import { Mongo } from 'meteor/mongo';

export const Counters = new Mongo.Collection('counters');

Counters.attachSchema({
    next_val: {
        type: Number
    }
});
