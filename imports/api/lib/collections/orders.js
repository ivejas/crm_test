import { Mongo } from 'meteor/mongo';

export const Orders = new Mongo.Collection('orders');

Orders.attachSchema({
    product_id: {
        type: String
    },
    customer_id: {
        type: String
    },
    quantity: {
        type: Number
    },
    order_date: {
        type: String
    },
    status: {
        type: String
    },
    order_id: {
        type: Number
    },
    added_by: {
        type: String
    },
    manager_id: {
        type: String
    },
    customer_name_cache: {
        type: Object,
        blackbox: true,
        optional: true
    },
    product_name_cache: {
        type: Object,
        blackbox: true,
        optional: true
    },
    manager_name_cache: {
        type: Object,
        blackbox: true,
        optional: true
    }
});
