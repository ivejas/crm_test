import {Customers} from './collections/customers.js';
import {Products} from './collections/products.js';
import {Orders} from './collections/orders.js';

import {migrate} from 'meteor/herteby:denormalize'


Orders.addLinks({
    'customer': {
        type: 'one',
        collection: Customers,
        field: 'customer_id', 
        autoremove: true,     
        denormalize: {
            field: "customer_name_cache",
            body: {
                first_name: 1,
                last_name: 1
            }
        }
    },
    'product': {
        type: 'one',
        collection: Products,
        field: 'product_id',     
        denormalize: {
            field: "product_name_cache",
            body: {
                name: 1
            }
        }
    },
    'manager': {
        type: 'one',
        collection: Meteor.users,
        field: 'manager_id'
    }
});
migrate('orders', 'customer_name_cache');
migrate('orders', 'product_name_cache');

Customers.addLinks({
    'customer_orders': {
        collection: Orders,
        inversedBy: 'customer',
        autoremove: true,
    }   
});

Products.addLinks({
    'product_orders': {
        collection: Orders,
        inversedBy: 'product',
        autoremove: true,
    }
});

Meteor.users.addLinks({
    'manager_orders': {
        collection: Orders,
        inversedBy: 'manager'
    }
});

Customers.addReducers({
    full_name: {
        body: {
            first_name: 1,
            last_name: 1
        },
        reduce(object) {

            return `${object.first_name} ${object.last_name}`;
        }
    }
})


Meteor.users.addReducers({
    full_name: {
        body: {
            profile: {
                first_name: 1,
                last_name: 1
            }
        },
        reduce(object) {
            const {profile} = object;

            return `${profile.first_name} ${profile.last_name}`;
        }
    }
});
