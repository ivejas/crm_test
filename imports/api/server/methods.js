import { Products } from '../lib/collections/products.js';
import { Customers } from '../lib/collections/customers.js';
import { Counters } from '../lib/collections/counters.js';
import { Orders } from '../lib/collections/orders.js';

Meteor.methods({
    "register" (data) {
        let user_id = Accounts.createUser(data);
        return user_id;
    },
    "insertProduct" (data) {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        data.item_id = incrementCounter(Counters, "item_id");
        data.added_by = this.userId;
        let product = Products.insert(data);
        return product;
    },
    "updateProduct" (data, id) {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        data.added_by = this.userId;
        let product = Products.update(id, {$set: data});
        return product;
    },
    "deleteProduct" (id) {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        let removed = Products.remove(id);
        return removed;
    },
    "insertCustomer" (data) {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        data.customer_id = incrementCounter(Counters, "customer_id");
        data.added_by = this.userId;
        let customer = Customers.insert(data);
        return customer;
    },
    "updateCustomer" (data, id) {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        data.added_by = this.userId;
        let customer = Customers.update(id, {$set: data});
        return customer;
    },
    "deleteCustomer" (id) {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        let removed = Customers.remove(id);
        return removed;
    },
    "insertOrder" (data) {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        data.order_id = incrementCounter(Counters, "order_id");
        data.added_by = this.userId;
        let order = Orders.insert(data);
        return order;
    },
    "updateOrder" (data, id) {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        data.added_by = this.userId;
        let order = Orders.update(id,{$set: data});
        return order;
    },
    "deleteOrder" (id) {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        let removed = Orders.remove(id);
        return removed;
    },
    "getManagers" () {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        let managers = Meteor.users.createQuery({
            full_name: 1
        });
        return managers.fetch();
    },
    "getOrders" (page = 1, perPage = 10) {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        let orders = Orders.createQuery({
            $paginate: true,
            quantity: 1,
            product_id: 1,
            order_date: 1,
            status: 1,
            order_id: 1,
            product_name_cache: {
                name: 1
            },
            manager: {
                full_name: 1
            },
            customer_name_cache: {
                first_name: 1,
                last_name: 1
            }
        });
        let count = orders.getCount();
        return [orders.clone({
            limit: perPage,
            skip: (page - 1) * 1
        }).fetch(), count];
    },
    "searchOrdersByCustomerFirstName" (string) {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        let orders = Orders.createQuery({
            $filters: {'customer_name_cache.first_name': {$regex: ".*"+string+".*", "$options": "i"}},
            quantity: 1,
            product_id: 1,
            order_date: 1,
            status: 1,
            order_id: 1,
            product_name_cache: {
                name: 1
            },
            manager: {
                full_name: 1
            },
            customer_name_cache: {
                first_name: 1,
                last_name: 1
            }
        });
        let ordersList = orders.fetch();
        if(ordersList.length == 0) {
            let orders = Orders.createQuery({
                quantity: 1,
                product_id: 1,
                order_date: 1,
                status: 1,
                order_id: 1,
                product_name_cache: {
                    name: 1
                },
                manager: {
                    full_name: 1
                },
                customer_name_cache: {
                    first_name: 1,
                    last_name: 1
                }
            });
            let ordersList = orders.fetch();
            let levenshteinArray = [];
            for(let x = 0; x < ordersList.length; x++) {
                let firstNameDistance = Levenshtein.get(string, ordersList[x].customer_name_cache.first_name);
                if(firstNameDistance < 4) {
                    ordersList[x].levenshtein = firstNameDistance;
                    levenshteinArray.push(ordersList[x]);
                }
            }
            levenshteinArray = levenshteinArray.sort(function(a, b) {
                return a.levenshtein - b.levenshtein;
            });
            return levenshteinArray;
        }
        return ordersList;
    },
    "searchOrdersByCustomerLastName" (string) {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        let orders = Orders.createQuery({
            $filters: {'customer_name_cache.last_name': {$regex: ".*"+string+".*", "$options": "i"}},
            quantity: 1,
            product_id: 1,
            order_date: 1,
            status: 1,
            order_id: 1,
            product_name_cache: {
                name: 1
            },
            manager: {
                full_name: 1
            },
            customer_name_cache: {
                first_name: 1,
                last_name: 1
            }
        });
        let ordersList = orders.fetch();
        if(ordersList.length == 0) {
            let orders = Orders.createQuery({
                quantity: 1,
                product_id: 1,
                order_date: 1,
                status: 1,
                order_id: 1,
                product_name_cache: {
                    name: 1
                },
                manager: {
                    full_name: 1
                },
                customer_name_cache: {
                    first_name: 1,
                    last_name: 1
                }
            });
            let ordersList = orders.fetch();
            let levenshteinArray = [];
            for(let x = 0; x < ordersList.length; x++) {
                let lastNameDistance = Levenshtein.get(string, ordersList[x].customer_name_cache.last_name);
                if(lastNameDistance < 4) {
                    ordersList[x].levenshtein = lastNameDistance;
                    levenshteinArray.push(ordersList[x]);
                }
            }
            levenshteinArray = levenshteinArray.sort(function(a, b) {
                return a.levenshtein - b.levenshtein;
            });
            return levenshteinArray;
        }
        return ordersList;
    },
    "searchOrdersByProduct" (string) {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        let orders = Orders.createQuery({
            $filters: {'product_name_cache.name': {$regex: ".*"+string+".*", "$options": "i"}},
            quantity: 1,
            product_id: 1,
            order_date: 1,
            status: 1,
            order_id: 1,
            product_name_cache: {
                name: 1
            },
            manager: {
                full_name: 1
            },
            customer_name_cache: {
                first_name: 1,
                last_name: 1
            }
        });
        let ordersList = orders.fetch();
        if(ordersList.length == 0) {
            let orders = Orders.createQuery({
                quantity: 1,
                product_id: 1,
                order_date: 1,
                status: 1,
                order_id: 1,
                product_name_cache: {
                    name: 1
                },
                manager: {
                    full_name: 1
                },
                customer_name_cache: {
                    first_name: 1,
                    last_name: 1
                }
            });
            let ordersList = orders.fetch();
            let levenshteinArray = [];
            for(let x = 0; x < ordersList.length; x++) {
                let lastNameDistance = Levenshtein.get(string, ordersList[x].product_name_cache.name);
                if(lastNameDistance < 4) {
                    ordersList[x].levenshtein = lastNameDistance;
                    levenshteinArray.push(ordersList[x]);
                }
            }
            levenshteinArray = levenshteinArray.sort(function(a, b) {
                return a.levenshtein - b.levenshtein;
            });
            return levenshteinArray;
        }
        return ordersList;
    },
    "getOrder" (id) {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        let orders = Orders.createQuery({
            $filters: {_id: id},
            quantity: 1,
            product_id: 1,
            order_date: 1,
            status: 1,
            order_id: 1,
            customer: {
                full_name: 1,
                phone: 1,
                email: 1,
                address: 1,
                city: 1,
                state: 1,
                country: 1
            },
            product: {
                name: 1,
                price: 1,
                width: 1,
                height: 1,
                weight: 1
            },
            manager: {
                full_name: 1
            }
        });
        return orders.fetchOne();
    },
    "getCustomerNames" () {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        let customers = Customers.createQuery({
            full_name: 1
        });
        return customers.fetch();

    },
    "getCustomerList" () {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        let customers = Customers.createQuery({
            first_name: 1,
            last_name: 1,
            phone: 1,
            email: 1,
            city: 1,
            country: 1,
            customer_id: 1
        });
        return customers.fetch();
    },
    "getCustomer" (id) {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        let customers = Customers.createQuery({
            $filters: {_id: id},
            full_name: 1,
            first_name: 1,
            last_name: 1,
            phone: 1,
            email: 1,
            address: 1,
            city: 1,
            country: 1,
            state: 1,
            customer_id: 1,
            customer_orders: {
                product: {
                    name: 1
                },
                order_date: 1,
                status: 1,
                manager: {
                    full_name: 1
                },
                customer: {
                    full_name: 1
                }
            }
        });
        return customers.fetchOne();
    },
    "getProductNames" () {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        let products = Products.createQuery({
            name: 1
        });
        return products.fetch();
    },
    "getProducts" () {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        let products = Products.createQuery({
            item_id: 1,
            name: 1,
            price: 1,
            width: 1,
            height: 1,
            weight: 1,
            category: 1
        });
        return products.fetch();
    },
    "getProduct" (id) {
        if(this.userId == null) {
            throw new Meteor.Error(500, 'Error 500: User not found', 'the user is not found');
        }
        let products = Products.createQuery({
            $filters: {_id: id},
            name: 1,
            price: 1,
            width: 1,
            height: 1,
            weight: 1,
            category: 1,
            product_orders: {
                order_date: 1,
                quantity: 1,
                status: 1,
                manager: {
                    full_name: 1
                },
                customer: {
                    full_name: 1
                }
            }
        });
        return products.fetchOne();
    },
    "getMyProfile" () {
        let user =  Meteor.users.createQuery({
            $filters: {_id: this.userId},
            manager_orders: {
                order_date: 1,
                quantity: 1,
                status: 1,
                manager: {
                    full_name: 1
                },
                customer: {
                    full_name: 1
                }
            },
            full_name: 1
        });
        return user.fetchOne();
    }
});