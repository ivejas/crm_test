import { Customers } from '../../lib/collections/products.js';

Customers.allow({
    insert: function(userId, doc) {
        return false;
    },
    update: function(userId, doc, fieldNames, modifier) {
        return false;
    },
    remove: function(userId, doc) {
        return false;
    }

})

Meteor.publish('customers', function() {
    if(this.userId != null) {
        return Customers.find();
    }
    return this.ready();
});