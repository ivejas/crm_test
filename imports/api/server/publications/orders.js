import { Orders } from '../../lib/collections/orders.js';

Orders.allow({
    insert: function(userId, doc) {
        return false;
    },
    update: function(userId, doc, fieldNames, modifier) {
        return false;
    },
    remove: function(userId, doc) {
        return false;
    }

})

Meteor.publish('orders', function() {
    if(this.userId != null) {
        return Orders.find();
    }
    return this.ready();
});