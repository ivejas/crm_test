import { Products } from '../../lib/collections/products.js';

Orders.allow({
    insert: function(userId, doc) {
        return false;
    },
    update: function(userId, doc, fieldNames, modifier) {
        return false;
    },
    remove: function(userId, doc) {
        return false;
    }

})

Meteor.publish('products', function() {
    if(this.userId != null) {
        return Products.find();
    }
    return this.ready();
});