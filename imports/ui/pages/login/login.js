import './login.html';
import './login.css';

Template.login.onCreated(() => {

});

Template.login.onRendered(() => {

});

Template.login.onDestroyed(() => {

});

Template.login.helpers({
    loggedIn: () => {
        if(Meteor.userId() != null) {
            return true;
        }
        return false;
    }
});

Template.login.events({
    "submit form": (event) => {
        event.preventDefault();
        Meteor.loginWithPassword(event.currentTarget.email.value, event.currentTarget.password.value, (error, data) => {
            if(error != null) {
                Bert.alert(error.reason, 'danger', 'growl-top-right');
            } else {
                Bert.alert('Welcome', 'success', 'growl-top-right');
                FlowRouter.go('/orders');
            }
        })
    }
});