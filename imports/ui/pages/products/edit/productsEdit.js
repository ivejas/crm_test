import './productsEdit.html';
import { Session } from 'meteor/session'

Template.productsEdit.onCreated(() => {
    Meteor.call('getProduct', FlowRouter.getParam("id"), (error, data) => {
        if(error != null) {
            Bert.alert(error.reason, 'danger', 'growl-top-right');
        } else {
            Session.set('data', data);
        }
    });

    Session.set('currentPage', 'products');
});

Template.productsEdit.onDestroyed(() => {
    delete Session.keys['data'];
});

Template.productsEdit.helpers({
    product: () => {
        return Session.get('data');
    },
    selected: (value1, value2) => {
        if(value1 == value2) {
            return true;
        }
        return false;
    }
});

Template.productsEdit.events({
    'submit form': (event) => {
        event.preventDefault();
        if(event.currentTarget.category.value == 'none') {
            Bert.alert('Category not selected', 'danger', 'growl-top-right');
            return false;
        }
        let productData = {
            name: event.currentTarget.name.value,
            price: event.currentTarget.price.value,
            height: event.currentTarget.height.value,
            width: event.currentTarget.width.value,
            weight: event.currentTarget.weight.value,
            category: event.currentTarget.category.value,
        }
        let id = Session.get('data')._id;
        Meteor.call('updateProduct', productData, id, (error, data) => {
            if(error != null) {
                Bert.alert(error.reason, 'danger', 'growl-top-right');
            } else {
                Bert.alert('Product updated', 'success', 'growl-top-right');
                FlowRouter.go('/products');
            }
        });
    }
});