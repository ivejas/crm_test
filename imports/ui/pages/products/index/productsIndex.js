import './productsIndex.html';

import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session'
import { Products } from '../../../../api/lib/collections/products.js';

Template.productsIndex.onCreated(() => {
    Meteor.call('getProducts', (error, data) => {
        if(error != null) {
            Bert.alert(error.reason, 'danger', 'growl-top-right');
        } else {
            Session.set('data', data);
        }
    });

    Session.set('currentPage', 'products');
});

Template.productsIndex.onDestroyed(() => {
    delete Session.keys['data'];
});

Template.productsIndex.helpers({
    "products": () => {
        return Session.get('data');
    },
    "upperCase": (string) => {
        return string.replace(new RegExp('^'+string[0]+''), string[0].toUpperCase());
    }
});

Template.productsIndex.events({
    'click .delete': (event) => {
        let toggle = confirm('Delete this order?');
        if(toggle == true) {
            Meteor.call('deleteProduct', event.currentTarget.getAttribute('data-id'), (error,data) => {
                if(error != null) {
                    Bert.alert(error.reason, 'danger', 'growl-top-right');
                } else {
                    Meteor.call('getProducts', (error, data) => {
                        if(error != null) {
                            Bert.alert(error.reason, 'danger', 'growl-top-right');
                        } else {
                            Bert.alert('Product deleted', 'success', 'growl-top-right');
                            Session.set('data', data);
                        }
                    });
                }
            });
        }
    }
});