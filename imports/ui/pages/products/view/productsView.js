import './productsView.html';

import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session'

Template.productsView.onCreated(() => {
    Meteor.call('getProduct', FlowRouter.getParam("id"), (error, data) => {
        if(error != null) {
            Bert.alert(error.reason, 'danger', 'growl-top-right');
        } else {
            Session.set('data', data);
        }
    });

    Session.set('currentPage', 'orders');
});

Template.productsView.onDestroyed(() => {
    delete Session.keys['data'];
});


Template.productsView.helpers({
    "product": () => {
        return Session.get('data');
    },
    "upperCase": (string) => {
        return string.replace(new RegExp('^'+string[0]+''), string[0].toUpperCase());
    }
});