import './productsAdd.html';
import { Session } from 'meteor/session'

Template.productsAdd.onCreated(() => {
    Session.set('currentPage', 'products');

});

Template.productsAdd.events({
    'submit form': (event) => {
        event.preventDefault();
        if(event.currentTarget.category.value == 'none') {
            Bert.alert('Category not selected', 'danger', 'growl-top-right');
            return false;
        }
        let productData = {
            name: event.currentTarget.name.value,
            price: event.currentTarget.price.value,
            height: event.currentTarget.height.value,
            width: event.currentTarget.width.value,
            weight: event.currentTarget.weight.value,
            category: event.currentTarget.category.value,
        }
        Meteor.call('insertProduct', productData, (error, data) => {
            if(error != null) {
                Bert.alert(error.reason, 'danger', 'growl-top-right');
            } else {
                Bert.alert('Product added', 'success', 'growl-top-right');
                FlowRouter.go('/products');
            }
        });
    }
});