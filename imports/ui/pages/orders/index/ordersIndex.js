import './ordersIndex.html';

import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session'

let timeout = null;

Template.ordersIndex.onCreated(() => {
    Meteor.call('getOrders', 1, 10, (error, data) => {
        if(error != null) {
            Bert.alert(error.reason, 'danger', 'growl-top-right');
        } else {
            Session.set('data', data[0]);
            Session.set('dataCount', data[1]);
            Session.set('page', 1);
        }
    });

    Session.set('currentPage', 'orders');
});

Template.ordersIndex.onDestroyed(() => {
    delete Session.keys['data'];
    delete Session.keys['dataCount'];
    delete Session.keys['page'];
    delete Session.keys['searching'];
    delete Session.keys['searching_first_name'];
    delete Session.keys['searching_last_name'];
    delete Session.keys['searching_product'];
});

Template.ordersIndex.helpers({
    "orders": () => {
        return Session.get('data');
    },
    "getName": (data) => {
        return data.customer.first_name+' '+data.customer.last_name
    },
    "isActive": (page) => {
        if(page == Session.get('page')) {
            return 'active';
        }
        return false;
    },
    "paginationPages": () => {
        let count = Math.ceil(Session.get('dataCount') / 10);
        let counters = [];
        for(let x = 1; x <= count; x++) {
            counters.push(x);
        }
        return counters;
    },
    "searching": () => {
        return Session.get('searching');
    }
});

Template.ordersIndex.events({
    'click .delete': (event) => {
        let toggle = confirm('Delete this order?');
        if(toggle == true) {
            Meteor.call('deleteOrder', event.currentTarget.getAttribute('data-id'), (error,data) => {
                if(error != null) {
                    Bert.alert(error.reason, 'danger', 'growl-top-right');
                } else {
                    Meteor.call('getOrders', 1, 10, (error, data) => {
                        if(error != null) {
                            Bert.alert(error.reason, 'danger', 'growl-top-right');
                        } else {
                            Session.set('data', data[0]);
                            Session.set('dataCount', data[1]);
                            Session.set('page', 1);
                        }
                    });
                }
            });
        }
    },
    'keyup #customer_first_name_search': (event) => {
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            if(event.currentTarget.value == "") {
                Session.set('searching_first_name', false);
            } else {
                Session.set('searching_first_name', true);
            }
            if(Session.get('searching_first_name') == false && Session.get('searching_last_name') == false && Session.get('searching_product') == null) {
                Session.set('searching', false);
            } else {
                Session.set('searching', true);
            }
            Meteor.call('searchOrdersByCustomerFirstName', event.currentTarget.value, (error,data) => {
                if(error != null) {
                    Bert.alert(error.reason, 'danger', 'growl-top-right');
                } else {
                    Session.set('data', data);
                }
            })
        }, 500);
    },
    'keyup #customer_last_name_search': (event) => {
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            if(event.currentTarget.value == "") {
                Session.set('searching_last_name', false);
            } else {
                Session.set('searching_last_name', true);
            }
            if(Session.get('searching_first_name') == false && Session.get('searching_last_name') == false && Session.get('searching_product') == null) {
                Session.set('searching', false);
            } else {
                Session.set('searching', true);
            }
            Meteor.call('searchOrdersByCustomerLastName', event.currentTarget.value, (error,data) => {
                if(error != null) {
                    Bert.alert(error.reason, 'danger', 'growl-top-right');
                } else {
                    Session.set('data', data);
                }
            })
        }, 500);
    },
    'keyup #product_search': (event) => {
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            if(event.currentTarget.value == "") {
                Session.set('searching_product', false);
            } else {
                Session.set('searching_product', true);
            }
            if(Session.get('searching_first_name') == false && Session.get('searching_last_name') == false && Session.get('searching_product') == null) {
                Session.set('searching', false);
            } else {
                Session.set('searching', true);
            }
            Meteor.call('searchOrdersByProduct', event.currentTarget.value, (error,data) => {
                if(error != null) {
                    Bert.alert(error.reason, 'danger', 'growl-top-right');
                } else {
                    Session.set('data', data);
                }
            })
        }, 500);
    },
    'click .page-item.page': (event) => {
        event.preventDefault();
        let newPage = event.currentTarget.getElementsByClassName('page-link')[0].getAttribute('data-page');
        let pages = document.getElementsByClassName('page-item');
        for(let x=0; x<pages.length; x++) {
            pages[x].classList.remove('active');
        }
        event.currentTarget.classList.add('active');
        Meteor.call('getOrders', newPage, 1, (error, data) => {
            if(error != null) {
                Bert.alert(error.reason, 'danger', 'growl-top-right');
            } else {
                Session.set('data', data[0]);
                Session.set('page', newPage);
            }
        });
    },
    'click .next-page': (event) => {
        event.preventDefault();
        let newPage = Session.get('page') + 1;
        if(newPage > Math.ceil(Session.get('dataCount') / 10)) {
            return false;
        }
        let pages = document.getElementsByClassName('page-item');
        for(let x=0; x<pages.length; x++) {
            pages[x].classList.remove('active');
        }
        document.querySelectorAll('[data-page="'+newPage+'"]')[0].classList.add('active');
        Meteor.call('getOrders', newPage, 1, (error, data) => {
            if(error != null) {
                Bert.alert(error.reason, 'danger', 'growl-top-right');
            } else {
                Session.set('data', data[0]);
                Session.set('page', newPage);
            }
        });
    },
    'click .previous-page': (event) => {
        event.preventDefault();
        let newPage = Session.get('page') - 1;
        if(newPage < 1) {
            return false;
        }
        let pages = document.getElementsByClassName('page-item');
        for(let x=0; x<pages.length; x++) {
            pages[x].classList.remove('active');
        }
        document.querySelectorAll('[data-page="'+newPage+'"]')[0].classList.add('active');
        Meteor.call('getOrders', newPage, 1, (error, data) => {
            if(error != null) {
                Bert.alert(error.reason, 'danger', 'growl-top-right');
            } else {
                Session.set('data', data[0]);
                Session.set('page', newPage);
            }
        });
    }, 
    'click .reset': (event) => {
        Session.set('searching', false);
        Session.set('searching_product', false);
        Session.set('searching_first_name', false);
        Session.set('searching_last_name', false);
        document.getElementById('customer_first_name_search').value = "";
        document.getElementById('customer_last_name_search').value = "";
        document.getElementById('product_search').value = "";
        Meteor.call('getOrders', 1, 10, (error, data) => {
            if(error != null) {
                Bert.alert(error.reason, 'danger', 'growl-top-right');
            } else {
                Session.set('data', data[0]);
                Session.set('dataCount', data[1]);
                Session.set('page', 1);
            }
        });
    }
});