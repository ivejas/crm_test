import './ordersEdit.html';
import { Session } from 'meteor/session'
import { Customers } from '../../../../api/lib/collections/customers.js';
import { Products } from '../../../../api/lib/collections/products.js';

Template.ordersEdit.onCreated(() => {
    Session.set('currentPage', 'orders');
    Meteor.call('getProductNames', (error, data) => {
        if(error != null) {
            Bert.alert(error.reason, 'danger', 'growl-top-right');
        } else {
            Session.set('productNames', data);
        }
    });
    Meteor.call('getCustomerNames', (error, data) => {
        if(error != null) {
            Bert.alert(error.reason, 'danger', 'growl-top-right');
        } else {
            Session.set('customerNames', data);
        }
    });
    Meteor.call('getManagers', (error, data) => {
        if(error != null) {
            Bert.alert(error.reason, 'danger', 'growl-top-right');
        } else {
            Session.set('managers', data);
        }
    });
    Meteor.call('getOrder', FlowRouter.getParam("id"), (error, data) => {
        if(error != null) {
            Bert.alert(error.reason, 'danger', 'growl-top-right');
        } else {
            Session.set('data', data);
        }
    });

    Session.set('currentPage', 'orders');
});

Template.ordersEdit.onDestroyed(() => {
    delete Session.keys['customerNames'];
    delete Session.keys['productNames'];
    delete Session.keys['managers'];
    delete Session.keys['data'];
});

Template.ordersEdit.helpers({
    products: () => {
        return Session.get('productNames');
    },
    customers: () => {
        return Session.get('customerNames');
    },
    managers: () => {
        return Session.get('managers');
    },
    order: () => {
        return Session.get('data');
    },
    selected: (value1, value2) => {
        if(value1 == value2) {
            return true;
        }
        return false;
    }
});

Template.ordersEdit.events({
    'submit form': (event) => {
        event.preventDefault();
        
        if(event.currentTarget.customer.value == 'none') {
            Bert.alert('Customer not selected', 'danger', 'growl-top-right');
            return false;
        }
        if(event.currentTarget.product.value == 'none') {
            Bert.alert('Product not selected', 'danger', 'growl-top-right');
            return false;
        }
        if(event.currentTarget.status.value == 'none') {
            Bert.alert('Status not selected', 'danger', 'growl-top-right');
            return false;
        }
        let orderData = {
            product_id: event.currentTarget.product.value,
            customer_id: event.currentTarget.customer.value,
            quantity: event.currentTarget.quantity.value,
            order_date: event.currentTarget.order_date.value,
            status: event.currentTarget.status.value,
            manager: event.currentTarget.manager.value,
        }
        let id = Session.get('data')._id;
        Meteor.call('updateOrder', orderData, id, (error, data) => {
            if(error != null) {
                Bert.alert(error.reason, 'danger', 'growl-top-right');
            } else {
                Bert.alert('Order saved', 'success', 'growl-top-right');
                FlowRouter.go('/orders/view/'+id);
            }
        });
    }
});