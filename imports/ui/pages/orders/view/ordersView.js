import './ordersView.html';

import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session'

Template.ordersView.onCreated(() => {
    Meteor.call('getOrder', FlowRouter.getParam("id"), (error, data) => {
        if(error != null) {
            Bert.alert(error.reason, 'danger', 'growl-top-right');
        } else {
            Session.set('data', data);
        }
    });

    Session.set('currentPage', 'orders');
});

Template.ordersView.onDestroyed(() => {
    delete Session.keys['data'];
});


Template.ordersView.helpers({
    "order": () => {
        return Session.get('data');
    }
});