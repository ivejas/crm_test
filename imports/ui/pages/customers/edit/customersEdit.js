import './customersEdit.html';
import { Session } from 'meteor/session'

Template.customersEdit.onCreated(() => {
    Meteor.call('getCustomer', FlowRouter.getParam("id"), (error, data) => {
        if(error != null) {
            Bert.alert(error.reason, 'danger', 'growl-top-right');
        } else {
            Session.set('data', data);
        }
    });
    Session.set('currentPage', 'customers');

});

Template.customersEdit.onDestroyed(() => {
    delete Session.keys['data'];
});

Template.customersEdit.helpers({
    customer: () => {
        return Session.get('data');
    }
});

Template.customersEdit.events({
    'submit form': (event) => {
        event.preventDefault();
        
        let customerData = {
            first_name: event.currentTarget.first_name.value,
            last_name: event.currentTarget.last_name.value,
            phone: event.currentTarget.phone.value,
            email: event.currentTarget.email.value,
            address: event.currentTarget.address.value,
            city: event.currentTarget.city.value,
            state: event.currentTarget.state.value,
            country: event.currentTarget.country.value,
        }
        let id = Session.get('data')._id;
        Meteor.call('updateCustomer', customerData, id, (error, data) => {
            if(error != null) {
                Bert.alert(error.reason, 'danger', 'growl-top-right');
            } else {
                Bert.alert('Customer updated', 'success', 'growl-top-right');
                FlowRouter.go('/customers');
            }
        });
    }
});