import './customersView.html';
import { Session } from 'meteor/session'

Template.customersView.onCreated(() => {
    Meteor.call('getCustomer', FlowRouter.getParam("id"), (error, data) => {
        if(error != null) {
            Bert.alert(error.reason, 'danger', 'growl-top-right');
        } else {
            Session.set('data', data);
        }
    });
    Session.set('currentPage', 'customers');

});

Template.customersView.helpers({
    customer: () => {
        return Session.get('data');
    }
});

Template.customersView.onDestroyed(() => {
    delete Session.keys['data'];
});
