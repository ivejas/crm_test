import './customersAdd.html';
import { Session } from 'meteor/session'

Template.customersAdd.onCreated(() => {
    Session.set('currentPage', 'customers');

});

Template.customersAdd.events({
    'submit form': (event) => {
        event.preventDefault();
        
        let customerData = {
            first_name: event.currentTarget.first_name.value,
            last_name: event.currentTarget.last_name.value,
            phone: event.currentTarget.phone.value,
            email: event.currentTarget.email.value,
            address: event.currentTarget.address.value,
            city: event.currentTarget.city.value,
            state: event.currentTarget.state.value,
            country: event.currentTarget.country.value,
        }
        Meteor.call('insertCustomer', customerData, (error, data) => {
            if(error != null) {
                Bert.alert(error.reason, 'danger', 'growl-top-right');
            } else {
                Bert.alert('Product added', 'success', 'growl-top-right');
                FlowRouter.go('/customers');
            }
        });
    }
});