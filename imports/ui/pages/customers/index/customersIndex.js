import './customersIndex.html';

import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session'

Template.customersIndex.onCreated(() => {
    Meteor.call('getCustomerList', (error, data) => {
        if(error != null) {
            Bert.alert(error.reason, 'danger', 'growl-top-right');
        } else {
            Session.set('data', data);
        }
    });
    Session.set('currentPage', 'customers');
});

Template.customersIndex.onDestroyed(() => {
    delete Session.keys['data'];
});

Template.customersIndex.helpers({
    "customers": () => {
        return Session.get('data');;
    }
});

Template.customersIndex.events({
    'click .delete': (event) => {
        let toggle = confirm('Delete this customer?');
        if(toggle == true) {
            Meteor.call('deleteCustomer', event.currentTarget.getAttribute('data-id'), (error,data) => {
                if(error != null) {
                    Bert.alert(error.reason, 'danger', 'growl-top-right');
                } else {
                    Meteor.call('getCustomerList', (error, data) => {
                        if(error != null) {
                            Bert.alert(error.reason, 'danger', 'growl-top-right');
                        } else {
                            Bert.alert('Customer deleted', 'success', 'growl-top-right');
                            Session.set('data', data);
                        }
                    });
                }
            });
        }
    }
})