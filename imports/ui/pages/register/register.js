import './register.html';

Template.register.onCreated(() => {

});

Template.register.events({
    "submit form": (event) => {
        event.preventDefault();
        if(event.currentTarget.password.value != event.currentTarget.password_repeat.value) {
            alert('error');
            Bert.alert('Passwords do not match', 'danger', 'growl-top-right');
            return false;
        }
        let data = {
            email: event.currentTarget.email.value,
            password: event.currentTarget.password.value,
            profile: {
                first_name: event.currentTarget.first_name.value,
                last_name: event.currentTarget.last_name.value,
            }
        }
        Meteor.call('register', data, (error,data) => {
            if(error != null) {
                Bert.alert(error.reason, 'danger', 'growl-top-right');
            } else {
                Bert.alert('Welcome! Please login to continue.', 'success', 'growl-top-right');
                FlowRouter.go('/');
            }
        });
    }
});