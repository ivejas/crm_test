import './profile.html';

import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session'

Template.profile.onCreated(() => {
    Session.set('currentPage', 'profile');
    Meteor.call('getMyProfile', (error, data) => {
        if(error != null) {
            Bert.alert(error.reason, 'danger', 'growl-top-right');
        } else {
            Session.set('data', data);
        }
    });
});

Template.profile.onDestroyed(() => {
    delete Session.keys['data'];
});

Template.profile.helpers({
    'profile': () => {
        return Session.get('data');
    }
});